@extends('layouts.app')


@section('content')

<div class="container">

<form class="w-50 my-5" method="post" action="{{  route('manual_credit') }}">
  {{ csrf_field() }}
  <div class="form-group">
    <h5 class="text-center display-4">Credit Customer</h5> 
    <label for="phone_no">Phone No</label>
    <input type="text" class="form-control" id="" name="msisdn" aria-describedby="emailHelp" placeholder="Enter phone no">
  </div>

  <div class="form-group">
    <label for="amount">Amount</label>
    <input type="number" class="form-control" id="" name="amount" value="00"  placeholder="Enter amount">
  </div>

   <div class="form-group">
    <label for="created_by">Created By</label>
    <input type="text" class="form-control" id="" name="created_by"  placeholder="Enter Manual">
  </div>

   <div class="form-group">
    <label for="date_won">Date Won</label>
    <input type="" class="form-control" id="" name="date_created"  placeholder="Enter date customer won here">
  </div>

  <button type="submit" class="btn btn-lg btn-dark">Credit Customer</button>

</form>

@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <p class="alert alert-danger">{{$error}}</p>
    @endforeach
@endif

</div>

@stop
