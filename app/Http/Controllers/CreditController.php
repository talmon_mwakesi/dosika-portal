<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CreditController extends Controller
{
    public function getManualCreditForm(){
        return view('credit');
    }

    public function creditCustomer(Request $request){

        $this->validate($request, [
            'msisdn' => 'required',
            'amount' => 'required|numeric',
            'created_by' => 'required',
            'date_created' => 'required'
        ]);

        $client = new \GuzzleHttp\Client();

        $params = [
            'json' =>
                [
                    'msisdn' => $request->msisdn,
                    'amount' => $request->amount,
                    'created_by' => $request->created_by,
                    'date_created' => $request->date_created
                ]
        ];

        try {
            $response = $client->post('http://104.155.17.94/lotto/supa4Api/api/v1/manual_credit', $params);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            // This is will catch all connection timeouts
            // Handle accordinly
            return redirect()->route('home')->with('error', 'System has timed out,sorry :(');

        } catch (\GuzzleHttp\Exception\ClientException $e) {

            return redirect()->route('home')->with('error', 'Something went wrong,please contact your system administrator');

        }  

        $response = $response->getBody()->getContents();
        
        $message = json_decode($response);
        
        // if($message->error == '1') {

        //     return redirect()->route('home')->with('error', 'Something went wrong,please contact your system administrator');            

        //  }

        return redirect()->route('home')->with('success', $message->description);


    }
}
