<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/manual_credit','CreditController@getManualCreditForm')->name('manual_credit');

Route::post('/manual_credit','CreditController@creditCustomer')->name('manual_credit');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
